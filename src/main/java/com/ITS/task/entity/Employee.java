package com.ITS.task.entity;

public class Employee {
    private long employeeNumber;

    private String name;

    private String phone;

    private String email;

    private String position;

    public Employee() {
    }


    public Employee(long employeeNumber, String name, String phone, String email, String position) {
        this.employeeNumber = employeeNumber;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.position = position;
    }

    public long getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(long employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
