package com.ITS.task.controller;

import ch.qos.logback.core.model.Model;
import com.ITS.task.entity.Employee;
import com.ITS.task.service.EmployeeService;
import jakarta.websocket.server.PathParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;


    @PostMapping("/employee/update")
    public String update(@RequestBody Employee employee) {
        String result = employeeService.update(employee);
        return result;
    }

    @GetMapping("/getByEmployeeNumber/{employeeNumber}")
    public Employee getByEmployeeNumber(@PathVariable("employeeNumber")Long employeeNumber) {
        Employee employee = employeeService.getByEmployeeNumber(employeeNumber);
        return employee;
    }

    @GetMapping("/getAll")
    public List<Employee> getAll() {
        List<Employee> employeeList = employeeService.getAll();
        return employeeList;
    }

    @GetMapping("/find/{employee}")
    public List<Employee> find(@RequestBody Employee employee) {
        List<Employee> employeeList = employeeService.get(employee);
        return employeeList;
    }
    @DeleteMapping("/employee/delete/{id}")
    public String delete(@PathVariable("id") Long id) {
        String result = employeeService.delete(id);
        return result;
    }
}
