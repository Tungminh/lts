package com.ITS.task.utils;

import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

@Component
public class Validate {
    public boolean checkValidateEmail(String email) {
        if (email == null || email.isEmpty())
            return true;
        Pattern pat = Pattern.compile(Constant.REGEX.EMAIL);
        return pat.matcher(email).matches();
    }

    public boolean checkValidatePhone(String phone) {
        if (phone == null || phone.isEmpty())
            return true;
        Pattern pat = Pattern.compile(Constant.REGEX.PHONE);
        return pat.matcher(phone).matches();
    }
}
