package com.ITS.task.service;

import com.ITS.task.entity.Employee;

import java.util.List;

public interface EmployeeService {
    String update(Employee employee);

    List<Employee> getAll();

    String delete(Long employeeNumber);

    Employee getByEmployeeNumber(Long employeeNumber);

    List<Employee> get(Employee employee);
}
