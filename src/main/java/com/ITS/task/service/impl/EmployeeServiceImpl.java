package com.ITS.task.service.impl;

import com.ITS.task.entity.Employee;
import com.ITS.task.repository.EmployeeRepository;
import com.ITS.task.service.EmployeeService;
import com.ITS.task.utils.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private Validate validate;

    @Override
    public String update(Employee employee) {
        if (!validate.checkValidateEmail(employee.getEmail())) return "email not right format,try again!";
        if (!validate.checkValidatePhone(employee.getPhone()))
            return "phone not right format, Try again!";
        if (Objects.nonNull(employee.getEmployeeNumber())) {
            employeeRepository.update(employee);
        } else {
            employeeRepository.register(employee);
        }
        return "successfully";
    }

    @Override
    public List<Employee> getAll() {
        List<Employee> employeeList = new ArrayList<>();
        employeeList = employeeRepository.findAll();
        return employeeList;
    }

    @Override
    public String delete(Long id) {
        employeeRepository.delete(id);
        return "successfully";
    }

    @Override
    public Employee getByEmployeeNumber(Long employeeNumber) {
        Employee employee = employeeRepository.findByEmployeeNumber(employeeNumber);
        return employee;
    }

    @Override
    public List<Employee> get(Employee employee) {
        List<Employee> employeeList1 = new ArrayList<>();
        employeeList1 = employeeRepository.find(employee);
        return employeeList1;
    }
}
